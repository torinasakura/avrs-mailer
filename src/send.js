import nodemailer from 'nodemailer'
import config from './config'
import logger from './logger'

export default async function send(to, subject, html) {
  const { from, ...options } = config.get('mailer')

  const transporter = nodemailer.createTransport(options)
  const mailOptions = { from, to, subject, html }

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        logger.error(error)
        reject(error)
      } else {
        logger.info(`Message sent: ${info.response}`)
        resolve(info)
      }
    })
  })
}
