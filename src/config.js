import nconf from 'nconf'

nconf.argv().env({ separator: '_', lowerCase: true })

nconf.defaults({
  redis: {
    host: 'redis',
  },
  mailer: {
    service: 'yandex',
    auth: {
      user: 'devmailersender',
      pass: 'Pt23sdfaE_r',
    },
    from: 'devmailersender@yandex.ru',
  },
})

export default nconf
