import redis from 'redis'
import config from './config'
import logger from './logger'
import * as templates from './templates'

export default function mailer(callback) {
  const client = redis.createClient(config.get('redis'))

  client.on('message', async (channel, message) => {
    if (channel === 'mailer') {
      try {
        const { id, to, data } = JSON.parse(message)
        const template = templates[id]

        if (template) {
          await template(to, data)
        } else {
          logger.error(`Template ${id} not found.`)
        }
      } catch (error) {
        logger.error(error)
      }
    }
  })

  client.on('subscribe', callback)

  client.on('error', error => logger.error(error))

  client.subscribe('mailer')
}
