import registration from './registration'
import resetPassword from './resetPassword'

export {
  registration,
  resetPassword,
}
