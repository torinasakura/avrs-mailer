import Oy from 'oy-vey'
import React from 'react'
import send from '../send'
import Registration from './components/Registration'

export default async function registration(to, { url, ...user }) {
  const html = Oy.renderTemplate(<Registration {...user} url={url} />, {
    title: 'Registration success.',
    previewText: 'Registration success.',
  })

  await send(to, 'Confirmation instructions', html)
}

