import React, { PropTypes } from 'react'
import { Table, TBody, TR, TD, A } from 'oy-vey'

const ResetPassword = ({ url }) => (
  <Table width="100%">
    <TBody>
      <TR>
        <TD>
          <A href={url}>Reset password</A>
        </TD>
      </TR>
    </TBody>
  </Table>
)

ResetPassword.propTypes = {
  url: PropTypes.string,
}

export default ResetPassword
