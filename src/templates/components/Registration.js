import React, { PropTypes } from 'react'
import { Table, TBody, TR, TD, A } from 'oy-vey'

const Registration = ({ firstName, url }) => (
  <Table width="100%">
    <TBody>
      <TR>
        <TD>
          Welcome {firstName}!
        </TD>
      </TR>
      <TR>
        <TD>
          Registration success.
          Please follow the link below to activate your account <A href={url}>Activate account</A>
        </TD>
      </TR>
    </TBody>
  </Table>
)

Registration.propTypes = {
  firstName: PropTypes.string,
  url: PropTypes.string,
}

export default Registration
