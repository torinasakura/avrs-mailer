import Oy from 'oy-vey'
import React from 'react'
import send from '../send'
import ResetPassword from './components/ResetPassword'

export default async function resetPassword(to, { url }) {
  const html = Oy.renderTemplate(<ResetPassword url={url} />, {
    title: 'Reset password.',
    previewText: 'Reset password.',
  })

  await send(to, 'Reset password', html)
}

